
Event Manager Block Module provides a block and page to help people fill open
volunteer slots. It extends both Event (http://drupal.org/project/event)
and Event Manager (http://drupal.org/project/event_manager)

Installation
------------

Copy event_manager_block to your module directory (either /modules or 
/sites/default/modules) and then enable on the admin modules page. Configuration
options live under admin/settings/event_manager/block. To the block can be made
visible by placing the block in your layout as normal.

Author
------
Mel Martin
mmartin@computer.org